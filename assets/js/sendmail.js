// $(document).ready(function(e){
//     $('#contact-quotes-form').submit(function(e){
//         e.preventDefault();
//         var form = $(this)[0];
//
//     })
// })
function format(state) {
        if (!state.id) { return state.text; }
        var $state = $(
         '<span>' + state.text + '</span>'
        );

        return $state;
}

function send_mail(form) {
    var send_button = $(form).find('#email-send-btn');
    send_button.button('loading');
    var formaction = $(form).attr( 'action' );
    var formData = new FormData($(form)[0]);

    $.ajax({
        url: formaction,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            form.reset();
            send_button.button('reset');
            swal({
                title: "Success!",
                text: "Mail Sent",
                timer: 3000,
                type: "success"
            });
        },
        error: function(error)
       {
           console.log(error);
       }
    });
}

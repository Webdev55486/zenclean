var Quotes = function () {
    var check_val = function () {
        $.fn.select2.defaults.set("theme", "bootstrap");

        function format(state) {
				if (!state.id) { return state.text; }
				var $state = $(
				 '<span>' + state.text + '</span>'
				);

				return $state;
		}

        if (jQuery().select2 && $('#premises-type-select').size() > 0) {
            $("#premises-type-select").select2({
                placeholder: 'Premises (Type)',
                templateResult: format,
                templateSelection: format,
                minimumResultsForSearch: -1,
                width: 'auto',
                escapeMarkup: function(m) {
                    return m;
                }
            });

            $(".select2-size").select2({
                placeholder: 'Size',
                minimumResultsForSearch: -1,
                width: null
            });

            $('#premises-type-select').on('select2:select', function (e) {
                var current_val = $(this).val();

                $(".select2-size").select2('destroy');
                $(".select2-size").html("<option></option>");

                if (current_val == 1) {
                    $(".select2-size").select2({
                        placeholder: 'Size',
                        minimumResultsForSearch: -1,
                        width: null,
                        data: ['Ask size of house ', '25M - 100M', '100M - 500M', '500M - 1500M', '1500M Above']
                    });
                }

                if (current_val == 2) {
                    $(".select2-size").select2({
                        placeholder: 'Size',
                        minimumResultsForSearch: -1,
                        width: null,
                        data: ['Ask size of office ', '25M - 100M', '100M - 500M', '500M - 1500M', '1500M Above']
                    });
                }

                if (current_val == 3) {
                    $(".select2-size").select2({
                        placeholder: 'Size',
                        minimumResultsForSearch: -1,
                        width: null,
                        data: ['Single Offices ', 'Multiple Offices', 'Double Story Building', 'Multi Storey Building', 'Block of Units', 'School', 'Depot', 'Hangar', 'Cafe/Restaurant', 'Warehouse', 'Other']
                    });
                }
            });
		}

         $('.fast-qouta-form').validate({
	            errorElement: 'div', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: true, // do not focus the last invalid input
	            ignore: "",
	            rules: {
                    name: {
                        required: true
                    },
                    premises_type: {
                        required: true
                    },
                    premises_size: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                    phone_number: {
                        required: true
                    },
                    post_code: {
                        required: true
                    },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
                    element.parent().append(error);
                    error.css('display', 'none');
	            },

	            submitHandler: function (form) {
                    send_mail(form);
	            }
	        });

            $('.select2-size').change(function () {
                $('.fast-qouta-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

            $('#premises-type-select').change(function () {
                $('.fast-qouta-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

			$('.fast-qouta-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.fast-qouta-form').validate().form()) {
                        var form = $('.fast-qouta-form')[0];
                        send_mail(form);
	                }
	                return false;
	            }
	        });
	}

    return {
        //main function to initiate the module
        init: function () {

            check_val();

        }
    };
}();

jQuery(document).ready(function() {
    Quotes.init();
});

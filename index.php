<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>JenClean</title>
        <link rel="stylesheet" href="./assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/plugins/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="./assets/plugins/select2/css/select2.min.css" />
        <link rel="stylesheet" href="./assets/plugins/select2/css/select2-bootstrap.min.css" />
        <link rel="stylesheet" href="./assets/plugins/sweetalert/sweetalert.css" />
        <link rel="stylesheet" href="./assets/css/style.css">
        <link rel="icon" type="image/gif" href="./assets/images/fave-icon.png">
    </head>
    <body>
        <header id="header" class="header">
            <div class="header-wrapper">
                <div id="masthead" class="header-main ">
                    <div class="header-inner flex-row container" role="navigation">
                        <div id="logo" class="flex-col logo">
                            <a href="" title="Zen Clean - Small Business Insurance Online Japanese" rel="home">
                                <img src="./assets/images/zenclean_logo.svg" class="header_logo header-logo lazyloading" alt="PSC Direct">
                            </a>
                        </div>
                        <div style="flex:1;width:100%;"></div>
                        <div class="phone-number-div">
                            <ul class="nav"><li><h2>
                                <a class="phone-number" href="tel:1800 946 000"><i class="fa fa-phone"></i> 0434 077 484</a>
                            </h2></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main id="main">
            <div class="section" id="section_background"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="large-text-container">
                            <img src="./assets/images/text-large.svg" alt="large">
                            <h4> <img src="./assets/images/text-small.svg" alt="small"> </h4>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="qouta-form-container">
                            <form class="fast-qouta-form" id="contact-quotes-form" action="./sendmail.php" method="post">
                                <h2>Get fast, free quotes</h2>
                                <h4>From an insurance expert.</h4>
                                <div class="form-group" style="margin-top:20px;">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="premises-type-select" name="premises_type" class="select2 form-control" name="">
                                                <option></option>
                                                <option value="1">Household (default)</option>
                                                <option value="2">Small Business (Office)</option>
                                                <option value="3">Commercial/Contract</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select id="premises-size-select" name="premises_size" class="select2-size form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Email" value="">
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <input type="tel" class="form-control" name="phone_number" placeholder="Phone" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="post_code" placeholder="PostCode" value="">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block btn-lg" id="email-send-btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Sending Mail">
                                    Help me with my cleaning requirements
                                </button>
                                <div class="secure-container">
                                    <img src="./assets/images/secure.png" alt="">
                                    <h1>
                                        <span class="big-text">Your security is our priority.</span><br />
                                        <span class="small-text">We guarantee 100% privacy. Your information is secure with us.</span>
                                    </h1>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer id="footer" class="footer-wrapper">
            <div class="absolute-footer dark medium-text-center small-text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="copyright-footer">
                                <p>Address: 210 Gt Eastern Highway, Glen Forrest W.A. 6071</p>
                                <p>ABN 27 277 839 850</p>
                                <p>Tel: 0434 077 484 </p>
                                <p>Email: info@zenclean.com.au </p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="social-container">
                                <ul>
                                    <li><a href="https://www.facebook.com/Zencleanwa-154539858528068/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.instagram.com/zencleanwa/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script type="text/javascript" src="./assets/plugins/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="./assets/plugins/select2/js/select2.full.min.js"></script>
        <script type="text/javascript" src="./assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="./assets/plugins/jquery-validation/js/additional-methods.min.js"></script>
        <script type="text/javascript" src="./assets/plugins/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript" src="./assets/js/validator.js"></script>
        <script type="text/javascript" src="./assets/js/sendmail.js"></script>

    </body>
</html>

<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require 'vendor/autoload.php';

if(isset($_POST['name']) && !empty($_POST['name'])) {$name = $_POST['name'];}
if(isset($_POST['email']) && !empty($_POST['email'])) {$email = $_POST['email'];}
if(isset($_POST['post_code']) && !empty($_POST['post_code'])) {$post_code = $_POST['post_code'];}
if(isset($_POST['phone_number']) && !empty($_POST['phone_number'])) {$phone_number = $_POST['phone_number'];}
if(isset($_POST['premises_type']) && !empty($_POST['premises_type'])) {$premises_type = $_POST['premises_type'];}
if(isset($_POST['premises_size']) && !empty($_POST['premises_size'])) {$premises_size = $_POST['premises_size'];}
if ($premises_type == 1) {
    $premises_type_text = "Household (default)";
}elseif ($premises_type == 2) {
    $premises_type_text = "Small Business (Office)";
}elseif ($premises_type ==3) {
    $premises_type_text = "Commercial/Contract";
}

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = '599xx.websitewelcome.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'quotes@zenclean.com.au';                 // SMTP username
    $mail->Password = 'Zenclean@2018';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('Zenclean@support.com', 'New Reservation');
    $mail->addAddress('chol.r@hotmail.com', 'Support');            // Name is optional

    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Help Me With My Insurance';
    $mail->Body    = '<span style="color:#4576be;font-weight:bold;"> Name: </span> <span>'.$name.'</span><br />
                    <span style="color:#4576be;font-weight:bold;"> Premises Type: </span> <span>'.$premises_type_text.'</span><br />
                    <span style="color:#4576be;font-weight:bold;"> Premises size: </span> <span>'.$premises_size.'</span><br />
                    <span style="color:#4576be;font-weight:bold;"> Email: </span> <span>'.$email.'</span><br />
                    <span style="color:#4576be;font-weight:bold;"> Phone Number: </span> <span>'.$phone_number.'</span><br />
                    <span style="color:#4576be;font-weight:bold;"> Post Code: </span> <span>'.$post_code.'</span><br />';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo "success";
} catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
